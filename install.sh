#! /bin/bash

npm install express
npm install raw-body
npm install mathjs
npm install async
npm install socket.io
npm install socket.io-client
#npm install json2mongo # convert strict mode syntax to js mode
npm install mongodb
npm install node-schedule
npm install jsonfile
npm install process
