/* Swarm acoustic (sensing and inference) service database server

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var express = require('express');
var app = express();
var server = require('http').Server(app);
var serviceAddr = require('./libs/serviceAddr');
var gridfsServlet = require('./libs/gridfsServlet');
var colServlet = require('./libs/colServlet');
var queryServlet = require('./libs/queryServlet');
//var inferServlet = require('./libs/inferServlet');
//var io = require('socket.io')(server);
//io = require('./libs/io')(io);
var schedule = require('node-schedule');
//var exec = require('child_process').exec;
var jsonfile = require('jsonfile');
var jsonLogFile = require('./libs/jsonLogFile');

// necessary header to operate cross-origin
app.use(function(req, res, next){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "POST,PUT,GET,OPTIONS");
	next();
});

// instantiates servlets
app.use('/gridfs', gridfsServlet);
app.use('/col', colServlet);
app.use('/query', queryServlet);
//app.use('/infer', inferServlet);

app.get('/', function(req,res){
	// status handler
	var clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log('status get from '+clientIP);

	res.send(serviceAddr+': OK');
});
app.get('/model', function(req,res){
	// models list handler
	var clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log('model get from '+clientIP);

	res.send(serviceAddr+': OK');
    /*
	console.log('root dir = '+__dirname);	
	var fs = require("fs"),
        path = require('path'),
		srcpath = __dirname+'/libs/models',
		dirList = [];

    fs.readdirSync(srcpath).filter(function(file) {
        if (fs.statSync(path.join(srcpath, file)).isDirectory()){
			dirList.push(file);
        } 
    });

	console.log(dirList);
	res.send(dirList);
    */
});

// init jobs
/*
exec('./libs/bin/resetModels.sh ./libs/data/featModels "car_horn gun_shot speech"', function(err, stdout, stderr){
	console.log('models reseted');
	if (err){
	  	console.log('error: '+err);
	} else{
	  	console.log('stdout: \n'+stdout);
		console.log('stderr: \n'+stderr);
	}
});
jsonfile.writeFile(jsonLogFile, [], function(err){
	if (err){
		console.log(err);
   	} else{
		console.log('log file initialized');
   	}
});
*/

// periodic jobs
//var j = schedule.scheduleJob('0 */1 * * *', function(){ // execute every 1 hour 
//var j = schedule.scheduleJob('*/15 * * * *', function(){ // execute every 15 minutes 
/*
    console.log('scheduled job ran');

	//var cmd = './libs/bin/run_helloworld.sh /usr/local/MCR/v81/';
	var cmd = './libs/bin/run_onlineTrainSVM.sh /usr/local/MCR/v81/ '+jsonLogFile+' ./libs/data/featModels "car_horn gun_shot speech" "ND"';
	exec(cmd, function(err, stdout, stderr){
	  	if (err){
			console.log('error: '+err);
		} else{
			console.log('stdout: \n'+stdout);
			console.log('stderr: \n'+stderr);
		}
	});
});
*/

// start the server
server.timeout = 600000;
server.listen(8080, serviceAddr, function(){
	console.log('Swarm acoustic db service started!');
});
