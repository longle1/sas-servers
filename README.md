## Overview
The (database and web) servers are components (highlighted in the figure below) in the overall SAS. The web server is only for demonstration, the main functionality is carried out by the database server.

![alt tag](https://bytebucket.org/longle1/sas-servers/raw/837e47c8aa00efbedc3c30f85926a5812643ff88/fig1.png)

This is the repo of the servers of swarm acoustic service servers (SAS's). The service is demonstrated at http://acoustic.ifp.illinois.edu.

The repo for the client is at https://github.com/longle2718/sas-client

The repo for the sensor is at https://bitbucket.org/longle1/sas-sensor

## Analysis
This service architecture is supported by the following resource management [theory](https://bitbucket.org/longle1/rmss).

## Installation

MongoDB v2.6.9 installation
```bash
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install -y mongodb-org=2.6.9 mongodb-org-server=2.6.9 mongodb-org-shell=2.6.9 mongodb-org-mongos=2.6.9 mongodb-org-tools=2.6.9
```
Install other dependencies
```bash
sudo apt-get install libkrb5-dev
sudo apt-get install npm
./install.sh # while inside sas-servers/
sudo npm install forever -g
```
json2mongo must be installed manually the avoid the missing bson warning
```bash
git clone https://github.com/long0612/json2mongo # while insider node_modules/
cd json2mongo
npm install
```
Verify installation is complete by
```bash
npm ls
```
## Start
Start the services using the following commands. (To run the web service, one must be root.)
```bash
./startWeb.sh
./startDb.sh
```
OR
```bash
sudo forever start sasWebServer.js <IP>
forever start -o out.log -e err.log sasDbServer.js <IP>
```

## Stop
Stop the services using the following commands.
```bash
./stop.sh
```
OR
```bash
sudo forever list
forever stop <ID>
```

## Disable firewall
Disable firewall on servers' listening ports
```bash
sudo ufw allow 80 # for the web server
sudo ufw allow 8080 # for the db server
sudo ufw status
sudo ufw delete allow 80 # if no web server is needed
```

## FAQs
* Install MatLab runtime to run with
```bash
wget http://www.mathworks.com/supportfiles/downloads/R2015a/deployment_files/R2015a/installers/glnxa64/MCR_R2015a_glnxa64_installer.zip
unzip MCR_R2015a_glnxa64_installer.zip -d MCR_R2015a
cd MCR_R2015a
sudo ./install -mode silent -agreeToLicense yes
```
Remember to compile matlab source to binary with the '-R -nodisplay' flag and install

* Missing libXt.so.6.
See the following links:

http://packages.ubuntu.com/search?mode=filename&suite=raring&section=all&arch=any&keywords=libXt.so.6&searchon=contents

http://www.mathworks.com/matlabcentral/answers/99739-can-i-run-an-application-created-with-matlab-compiler-on-linux-without-x11-or-a-display

```bash
sudo apt-get install libxt6
```

## Acknowledgement
This work was supported in part by the TerraSwarm Research Center, one of six centers supported by the STARnet phase of the Focus Center Research Program (FCRP) a Semiconductor Research Corporation program sponsored by MARCO and DARPA. 

## License
The MIT License

Copyright (c) 2015 Long Le

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
