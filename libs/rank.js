/* The ranking engine used by queryServlet.js. The goal is to put
 * an accurate score for each submitted event.

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var gridStore = require('mongodb').GridStore;
//var act = require('./actuator');
var math = require("mathjs");
var PredModel = require('./PredModel');

module.exports = {
	batchProc: function(classname, events, db, collection, callback){
		var _this = this;

		var model = new PredModel();
		if (typeof classname !== 'undefined'){
			model.fromClass(classname, function(model){
				// **** Ranking
				console.log('Rank '+classname);
				for (var j = 0; j <events.length;j++){
					_this.ranking(events[j], model);
				}
				// sort events based on score
				events.sort(function(a, b) {
					return -a.score + b.score;
				});
				
				callback(events);
			});
		} else{
			callback(events);
		}
	},
	ranking: function(eventj, model){
		console.log('ranking');
		if (typeof model.mu !== 'undefined'){
			// Use model to augment score
			eventj.score = model.predict(model, eventj.TFRidgeFeat);
		} else {
			//eventj.score = math.sum(eventj.TFRidgeFeat.slice(0,16));
			eventj.score = 0;
		}
	}
};

