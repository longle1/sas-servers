/* Gridfs servlet

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var express = require('express');
var rawBody = require('raw-body');
var mongoClient = require('mongodb').MongoClient;
var mongoAddr = require('./mongoAddr');
var gridStore = require('mongodb').GridStore;
//var io = require('socket.io-client');

var dbname;
var colname;
//var user;
var passwd;
var filename;
var body;

var gridfsServlet = express.Router();

//gridfsServlet.use('/:db/:col', function(req, res, next){
gridfsServlet.use(function(req, res, next){
	var clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log('gridfsServlet from '+clientIP);

	rawBody(req, undefined, function(err, str){
		if (err){
			res.status(500).send(err);
		}
		body = str;
		//console.log(body.toString());

		//dbname = req.params.db;
		//colname = req.params.col;
		dbname = req.query.dbname;
		colname = req.query.colname;
		//user = req.query.user;
		passwd = req.query.passwd;
		filename = req.query.filename;

		console.log(dbname+' '+colname+' '+passwd+' '+filename);
		next();
	});
});

gridfsServlet.delete('/', function(req, res){
	console.log('delete');
	
	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			res.status(500).send(err);
			db.close();
		}
		else{
			console.log('filename: '+filename);
			// check for valid password
			var collection = db.collection(colname+'.files');
			collection.find({'filename':filename,'metadata.key':passwd},{_id:1},function(err, obj){
				if (err){
					console.log(err);
					res.status(500).send(err);
					db.close();
				} else{
					// open gridStore
					var gs = new gridStore(db, filename, 'w', {
						'root': colname
					});
					gs.open(function(err, gs){
						if (err){
							console.log('failed to open');
							res.status(500).send(err);
							db.close();
						}
						else{
							gs.unlink(function(err){
								if (err){
									console.log('failed to unlink');
									res.status(500).send(err);
								} else{
									console.log('unlink successful');
									// there is no result returned
									res.send('file deleted');
								}

								db.close();
							});
						}
					});
				}
			})
		}
	});
});

//gridfsServlet.get('/:db/:col', function(req, res){
gridfsServlet.get('/', function(req, res){
	console.log('get');

	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			res.status(500).send(err);
			db.close();
		}
		else{
			console.log(filename);
			// check for valid password
			var collection = db.collection(colname+'.files');
			collection.find({'filename':filename,'metadata.key':passwd},{_id:1},function(err, obj){
				if (err){
					console.log(err);
					res.status(500).send(err);
					db.close();
				} else{
					var gs = new gridStore(db, filename, 'r', {
						'root': colname
					});
					gs.open(function(err, gs){
						if (err){
							console.log('failed to open');
							res.status(500).send(err);
							db.close();
						}
						else{
							gs.seek(0, function(){
								gs.read(function(err, data){
									if (err){
										console.log('failed to read');
										res.status(500).send(err);
									}
									else{
										console.log('read successfully');
										res.send(data)
									}

									db.close();
								});
							});
						}	
					});
				}
			});
		}
	});
});
//gridfsServlet.post('/:db/:col', function(req, res){
gridfsServlet.post('/', function(req, res){
	console.log('post');

	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			console.log('failed to connect to db '+dbname);
			res.status(500).send(err);
			db.close();
		}
		else{
			console.log(req.get('content-type'));
			var gs = new gridStore(db, undefined, filename, 'w', {
				'root': colname,
				'content-type': req.get('content-type'),
				'metadata':{
					//'clientIp': req.connection.remoteAddress,
					'key': passwd
				}
			});
			
			gs.open(function(err, gs){
				if (err){
					console.log('failed to open');
					res.status(500).send(err);
					gs.close(function(err, gs){
						db.close();
					});
				}
				else{
					var bodyBuf = null;
					try{
						bodyBuf = new Buffer(body);
					} catch (exc){
						console.log('invalid grid body: '+body);
						//res.status(500).send(exc); //can't set header after sent
						gs.close(function(err, gs){
							db.close();
						});
						return;
					}
					gs.write(bodyBuf, function(err, gs){
						if (err){
							console.log('failed to write');
							res.status(500).send(err)
						}
						else{
							console.log(filename+' inserted');
							res.send(filename+' inserted\n');

							/*
							var socket = io.connect('http://'+req.headers.host, {forceNew:true, reconnection:false});
							socket.on('connect', function(){
								socket.emit('newData', filename);
								socket.disconnect();
							});
							*/
						}
						gs.close(function(err, gs){
							db.close();
						});
					});
				}	
			});
		}
	});
});

module.exports = gridfsServlet;
