/* Overloaded class of the socket.io server instantiated in sasDbServer.js

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var mongoClient = require('mongodb').MongoClient;
var mongoAddr = require('./mongoAddr');
var rank = require('./rank');
var clients = require('./clients');
var json2mongo = require('json2mongo');
var async = require("async");

var dataArray = [];

module.exports = function(io){
	io.on('connection', function(socket){
		remAddr = socket.request.connection.remoteAddress;
		console.log('socket connected '+remAddr);
		
		//var locAddr = socket.handshake.headers.host.split(':').shift();
		//console.log('local addr '+locAddr);

		// only add client address, not server
		if (remAddr){
			if (! clients[remAddr])
				clients[remAddr] = {};
			clients[remAddr].socketid = socket.id;
		}
		console.dir(clients);

		// handle connection termination
		socket.on('disconnect', function(){
			console.log('socket disconnected');
			for (var key in clients){
				if (clients[key].socketid === socket.id){
					delete clients[key];
				}
			}
		});

		// sensor data
		socket.on('newData', function(filename){
			console.log('socket newData');
			sensorHandler(filename,io);
		});

		// sensor event
		socket.on('newEvent', function(filename){
			console.log('socket newEvent');
			sensorHandler(filename,io);
		});
	});
	return io;
};

function sensorHandler(filename,io){
	var idx = dataArray.indexOf(filename);
	if (idx == -1){
		dataArray.push(filename);
		console.log('pushed filename: '+filename);
	} else{
		dataArray.splice(idx,1); // remove 1 element at idx
		console.log('paired filename: '+filename);

		async.forEachOf(clients, function(client, key, cb){
			var dbname = client.args.dbname;
			var colname = client.args.colname;
			//var user = client.args.user;
			var passwd = client.args.passwd;
			var lim = client.args.lim;
			var classname = client.args.classname;
			var body = client.args.body;

			mongoClient.connect(mongoAddr+dbname, function(err, db){
				if (err){
					console.log(err);
					db.close();
					cb();
				}
				else{
					var collection = db.collection(colname);
					var mongoDoc = json2mongo(JSON.parse(body));
					mongoDoc.$and.push({'key':passwd,'filename':filename});// assume query structure is an $and of many things
					console.log(mongoDoc);
					var cursor = collection.find(mongoDoc, {});// return all fields, including _id
					cursor.limit(lim).toArray(function(err, events){
						console.log('# events '+events.length);
						if (err){
							console.log(err);
							cb();
						}
						else{
							rank.batchProc(classname, events, db, collection, function(new_events){
								console.log('send '+new_events.length+' events to '+client.socketid);

								io.to(client.socketid).emit('newResp', new_events);
								db.close();
								cb();
							});
						}
					});
				}
			});
		});
	}
}
