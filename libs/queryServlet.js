/* Query servlet

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var express = require('express');
var rawBody = require('raw-body');
var mongoClient = require('mongodb').MongoClient;
var mongoAddr = require('./mongoAddr');
//var rank = require('./rank');
var clients = require('./clients');
var json2mongo = require('json2mongo');
var fs = require('fs');
var exec = require('child_process').exec;
var async = require("async");
var process = require('process');

var dbname;
var colname;
//var user;
var passwd;
var lim;
var MAX_LIM = 1e3;
//var classname;
var body;

var queryServlet = express.Router();

queryServlet.use('/', function(req, res, next){
	var clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log('queryServlet from '+clientIP);

	rawBody(req, undefined, function(err, str){
		if (err){
			console.log(err);
			res.status(500).send(err);
			return;
		}
		body = str;
		console.log(body.toString());

		dbname = req.query.dbname;
		colname = req.query.colname;
		//user = req.query.user;
		passwd = req.query.passwd;
		lim = req.query.limit;
		if (lim)
			lim = Math.min(parseInt(lim), MAX_LIM);
		else
			lim = MAX_LIM;
		//classname = req.query.classname;
	
		next();
	});
});

queryServlet.post('/', function(req, res){
	console.log('post /');

	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			console.log(err);
			res.status(500).send(err);
			db.close();
			return;
		}

		try {
			var mongoDoc = json2mongo(JSON.parse(body));
			mongoDoc[0].$and.push({'key': passwd});
			console.log(mongoDoc[0]);
		}catch (exc){
			console.log(exc);
			res.status(500).send(exc);
			db.close();
			return;
		}

		var collection = db.collection(colname);
		// the first field is the query, the second one is the mask/filter
		//var cursor = collection.find(mongoDoc, {});// return all fields, including _id
		var cursor = collection.find(mongoDoc[0], mongoDoc[1]);
		cursor.limit(lim).toArray(function(err, events){
			if (err){
				res.status(500).send(err);
				db.close();
				return;
			}
			console.log('# events '+events.length);
			res.send(events);
			db.close();

			/*
			console.log('classname: '+classname);
			if (typeof classname === 'undefined' || classname === 'none' || !fs.existsSync(__dirname+'/models/'+classname)){
				// update score of the events array
				for (var k = 0; k < events.length;k++){
					events[k].score = 0;
				}
			} else{
				// template code
				for (var k = 0; k < events.length;k++){
					try{
						events[k].score = events[k].maxDur;
					} catch (exc){
						events[k].score = 0;
					}
				}
				
				// go to a model dir
				console.log(__dirname+'/models/'+classname);
				process.chdir(__dirname+'/models/'+classname);

				// write the data list
				events.forEach(function (v) { 
					console.log(v.filename);
					fs.appendFileSync('./dataList', v.filename + "\n");
				});

				// execute the model
				console.log('./main.sh'+' '+req.hostname+' '+dbname+' '+passwd);
				exec('./main.sh'+' '+req.hostname+' '+dbname+' '+passwd, function(err, stdout, stderr){
					// return to root
					process.chdir('../../../');

					// handle output results
					if (err){
						console.log('error: '+err);
					} else{
						console.log('stdout: \n'+stdout);
						console.log('stderr: \n'+stderr);
						var scores = stdout.split(',');

						// update score of the events array
						for (var k = 0; k < events.length;k++){
							console.log('scores['+k+'] = '+scores[k]);
							events[k].score = Number(scores[k]);
						}
					}
					res.send(events);
					db.close();
				});
			}
			*/
			
			/*
			rank.batchProc(classname, events, db, collection, function(new_events){
				res.send(new_events);
				db.close();
			});
			*/
		});
	});
});

/*
queryServlet.post('/register', function(req, res){
	console.log('post /register');
	remAddr = req.connection.remoteAddress;

	if (! clients[remAddr])
		clients[remAddr] = {};
	clients[remAddr].args = { // keys are not variables, but values are
		dbname:dbname,
		colname:colname,
		//user:user,
		passwd:passwd,
		lim:lim,
		classname:classname,
		body:body
	};
	//console.log(clients[remAddr]);

	console.log('registered');
	res.send('registered');
});
*/

module.exports = queryServlet;
