/* Inference servlet

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var express = require('express');
var rawBody = require('raw-body');
var mongoClient = require('mongodb').MongoClient;
var mongoAddr = require('./mongoAddr');
var infer = require('./infer');
var clients = require('./clients');
var json2mongo = require('json2mongo');

var dbname;
var colname;
//var user;
var passwd;
var classname;
var body;

var inferServlet = express.Router();

inferServlet.use('/', function(req, res, next){
	console.log('inferServlet');

	rawBody(req, undefined, function(err, str){
		if (err){
			res.status(500).send(err);
		}
		body = str;
		console.log('body '+body.toString());

		dbname = req.query.dbname;
		colname = req.query.colname;
		//user = req.query.user;
		passwd = req.query.passwd;
		classname = req.query.classname;
	
		next();
	});
});
inferServlet.post('/', function(req, res){
	console.log('post /');

	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			res.status(500).send(err);
			db.close();
		}
		else{
			var collection = db.collection(colname);
			try{
				var events = JSON.parse(body);
				if (events.constructor !== Array){
					events = [events];
				}
				var cursor = collection.find(json2mongo({'key':passwd,'filename':{'$in':events.map(function(it){return it.filename;})}}), {});
				cursor.toArray(function(err, events){
					console.log('# events '+events.length);

					infer.batchProc(classname, events, db, collection, function(new_events){
						res.send(new_events);
						db.close();
					});
				});
			} catch(err){
				console.log(err);

				res.status(500).send(err);
				db.close();
			}
		}
	});
});

module.exports = inferServlet;
