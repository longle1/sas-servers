/* Pub-sub based actuator/Philips Hue Light

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var EventEmitter = require('events').EventEmitter;
var http = require("http");

var BRIDGEIP='128.174.226.140';
var USERNAME='760f6fe5759c473d26b123f29e86b';
var LIGHTID='2';
var options = {
	host:BRIDGEIP,
	path: '/api/'+USERNAME+'/lights/'+LIGHTID+'/state',
	method: 'PUT',
	headers:{
		'Content-Type':'application/json'
	}
};

var lightOn = function(){
	console.log('lightOn');

	var req = http.request(options, function(res){
		res.on('data', function(chunk){
			console.log(chunk);
		});
	});
	req.on('error', function(e){
		console.log(e);
	});

	req.write('{"on": true}');
	req.end();
};

var lightOff = function(){
	console.log('lightOff');

	var req = http.request(options, function(res){
		res.on('data', function(chunk){
			console.log(chunk);
		});
	});
	req.on('error', function(e){
		console.log(e);
	});

	req.write('{"on": false}');
	req.end();
};

var lightDanger = function(hueVal){
	console.log('lightDanger');

	var req = http.request(options, function(res){
		res.on('data', function(chunk){
			console.log(chunk);
		});
	});
	req.on('error', function(e){
		console.log(e);
	});

	req.write('{"on":true, "hue":'+hueVal+', "effect":"colorloop"}');
	req.end();
};

var actuator = new EventEmitter();
actuator.on('lightOn', lightOn);
actuator.on('lightOff', lightOff);
actuator.on('lightDanger', lightDanger);

module.exports = actuator;
