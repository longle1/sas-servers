/* Write servlet

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var express = require('express');
var rawBody = require('raw-body');
var mongoClient = require('mongodb').MongoClient;
var mongoAddr = require('./mongoAddr');
var json2mongo = require('json2mongo');
//var io = require('socket.io-client');
//var rank = require('./rank');
//var clients = require('./clients');
//var serviceAddr = require('./serviceAddr');
//var jsonfile = require('jsonfile');
//var jsonLogFile = require('./jsonLogFile');

var dbname;
var colname;
//var user;
var passwd;
var classname;
var body;

var colServlet = express.Router();

colServlet.use(function(req, res, next){
	var clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log('colServlet from '+clientIP);

	rawBody(req, undefined, function(err, str){
		if (err){
			res.status(500).send(err);
		} else{
			body = str;
			//console.log(body.toString());

			dbname = req.query.dbname;
			colname = req.query.colname;
			//user = req.query.user;
			passwd = req.query.passwd;

			next();
		}
	});
});

colServlet.get('/', function(req, res){
	console.log('get');
	filename = req.query.filename;

	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			res.status(500).send(err);
			db.close();
		}
		else{
			var collection = db.collection(colname);
			var cursor = collection.find({'filename':filename,'key':passwd}, {});// return all fields, including _id
			cursor.toArray(function(err, events){
				if (err)
					res.status(500).send(err);
				else{
					console.log('# events '+events.length);
					res.send(events);
					db.close();
				}
			});
		}
	});
});

colServlet.delete('/', function(req, res){
	console.log('delete');
	filename = req.query.filename;
	
	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			res.status(500).send(err);
			db.close();
		}
		else{
			var collection = db.collection(colname);
			
			// find and update doc
			console.log('filename: '+filename);
			collection.remove({'filename':filename,'key':passwd}, {}, function(err, result){
				if(err){
					console.log('failed to remove');
					res.status(500).send(err);
				}
				else{
					console.log('remove successful');
					res.send(result);
				}
				db.close();
			});
		}
	});
});

colServlet.put('/', function(req, res){
	console.log('put');
	
	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			res.status(500).send(err);
			db.close();
		} else if (typeof body === 'undefined'){
			console.log('undefined body');
			res.status(500).send('undefined body');
			db.close();
		} else{
			var collection = db.collection(colname);
			
			// find and update doc
			critUp = body.toString().split('\n'); // a vector of criteria and update
			try{
				var critJson = JSON.parse(critUp[0]);
			} catch (exc){
				console.log(exc);
				res.status(500).send(exc);
				db.close();
				return;
			}
			critJson.key = passwd; // add key in critJson
			try{
				var upJson = JSON.parse(critUp[1]);
			} catch (exc){
				console.log(exc);
				res.status(500).send(exc);
				db.close();
				return;
			}
			console.log(critJson+' '+upJson);

			// main update
			collection.update(critJson, upJson, function(err, result){
				if(err){
					console.log('failed to update');
					res.status(500).send(err);
					db.close();
				}
				else{
					console.log('update successful');

					/*
					// log events with tag to a file
					if ( upJson.hasOwnProperty('$set') && upJson['$set'].hasOwnProperty('tag')){
						var cursor = collection.find(json2mongo(critJson));
						cursor.limit(1).each(function(err, doc){// limit to 1 to avoid read/write inconsistency
							if (err){
								console.log('find failed '+err);
							} else{
								// found the doc, ignore the null
								if (doc != null){
									jsonfile.readFile(jsonLogFile, function(err, obj){
										if (err){
											console.log(err);
										} else{
											var eventArr = obj.concat(doc);
											jsonfile.writeFile(jsonLogFile, eventArr, function(err){
												if (err){
													console.log(err); 
												} else{
													console.log('successfully log an event with tag');
												}	
											});
										}
									});
								}
							}
							db.close();
						});
					} else{
						db.close();
					}
					*/

					// finalizing the request
					res.send(result);
				}
			});
		}
	});
});

colServlet.post('/', function(req, res){
	console.log('post');

	mongoClient.connect(mongoAddr+dbname, function(err, db){
		if (err){
			res.status(500).send(err);
			db.close();
		} else if (typeof body === 'undefined'){
			console.log('undefined body');
			res.status(500).send('undefined body');
			db.close();
		}else{
			var collection = db.collection(colname);
			
			// add indexing if needed
			collection.indexExists('tag_text', function(err, result){
				console.log('result of index test: '+result);
				if (result === false){
					collection.createIndex({'tag':'text'}, undefined, function(err, idxname){
						if (err === null)
							console.log('Text index '+idxname+' created');
						else
							console.log('createIndex error '+err); // known issue: index created but error about topology being destroyed
					});
				}
			});

			// insert document, no options
			//console.log(serviceAddr);
			//jsonBody.serviceAddr = serviceAddr; // tag data with the current service address
			try{
				var jsonBody = JSON.parse(body);
			}catch (exc){
				console.log(exc);
				res.status(500).send(exc);
				db.close();
				return;
			}
			jsonBody.key = passwd;
			var mongoDoc = json2mongo(jsonBody);
			collection.insert(mongoDoc, undefined, function(err){
				if (err){
					console.log(err);
					res.status(500).send(err);
				}
				else{
					try {
			            console.log('mongoDoc '+mongoDoc.filename);
						var fnameParts = mongoDoc.filename.split(".");
						console.log(fnameParts[0]+' inserted');
						//console.log(req.headers.host);
						res.send(fnameParts[0]+' inserted\n');
						/*
						var socket = io.connect('http://'+req.headers.host, {forceNew:true, reconnection:false});
						socket.on('connect', function(){
							socket.emit('newEvent', mongoDoc.filename);
							socket.disconnect();
						});
						*/
					} catch (exc){
						console.log(exc);
						//res.status(500).send(exc);
						res.send('unnaned event inserted\n');
					}
				}
				db.close();
			});
		}
	});
});

module.exports = colServlet;
