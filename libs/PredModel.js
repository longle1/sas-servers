/* Prediction model used by the ranking engine in rank.js
 * 
 * The MIT License
 *
 * Copyright 2015 Long.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var fs = require('fs');
var path = require('path');
var math = require("mathjs");

var PredModel = function () {
		this.alpha;
		this.bias; // score bias
		this.kernelFcn;
		this.kernelScale;
		this.mu; // standardization mean
		this.sigma; // standardization sigma
		this.sv; // support vectos
		this.svl; // support vector labels
		this.sxfp; // score transform parameters
		console.log('model instance created');
};
PredModel.prototype = {
		// find the name of the json model from classname
		fromClass: function(classname, cb){
				var jsonFile;
				switch (classname){
            case 'car_horn':
                jsonFile = "data/featModels/car_horn.json";
                break;
            case 'dog_bark':
                jsonFile = "data/featModels/dog_bark.json";
                break;
            case 'drilling':
                jsonFile = "data/featModels/drilling.json";
                break;
            case 'gun_shot':
                jsonFile = "data/featModels/gun_shot.json";
                break;
            case 'siren':
                jsonFile = "data/featModels/siren.json";
                break;
            case 'speech':
                jsonFile = "data/featModels/speech.json";
                break;
						default:
								break;
        }
				if (typeof jsonFile !== 'undefined'){
					// save this PredModel to distinguish it from the "this" in getJSON's callback
					var _this = this;
					// cannot use $.getJSON
					/*
					$.getJSON(jsonFile, function(json){
							_this.fromJSON(json);
							cb(_this);
					});
					*/
					fs.readFile(path.join(__dirname,jsonFile), 'utf8', function(err, str){
						if (err) throw err;
						_this.fromJSON(JSON.parse(str));
						cb(_this);
					});
				} else{
					cb(this);
				}
		},
		// construct the prediction model from json model
		fromJSON: function(json){
				this.alpha = json.alpha;
				this.bias = json.bias;
				this.kernelFcn = json.kernelFcn;
				this.kernelScale= json.kernelScale;
				this.mu = json.mu;
				this.sigma = json.sigma;
				this.sv = json.sv;
				this.svl = json.svl;
				this.xformFcn = json.xformFcn;
				this.sxfp = json.sxfp;
		},
		// make class prediction for a given feat using the prediction model
		predict: function(model, feat){
				if (this.kernelFcn === 'gaussian'){
				  		// standardize
						var std_feat = math.dotDivide(math.subtract(feat, model.mu), model.sigma);

						// compute score
						wx = new Array(model.sv.length);
						for (k = 0; k < model.sv.length; k++){
								wx[k] = model.alpha[k]*model.svl[k]*math.exp(-math.pow(math.norm(math.subtract(model.sv[k], std_feat),2),2)/math.pow(model.kernelScale,2));
						}
						score = math.sum(wx)+model.bias;
						
						// apply Platt weighting
						if (model.xformFcn === 'sigmoid'){
								return sigmoid(score, model.sxfp[0], model.sxfp[1]);
						}
						else if (model.xformFcn === 'step'){
								return step(score, model.sxfp[0], model.sxfp[1], model.sxfp[2]);
						}
						else{
								return -1;
						}
				}
				else{
						console.log("ERROR! unrecognized kernel type." + this.kernelType);
						return null;
				}
		}
};

// sigmoid function
var sigmoid = function(x,a,b){
		return 1/(1+math.exp(a*x+b));
};

// step function
var step = function(x,lb,ub,pi){
		if (x < lb){
				return 0;
		}
		else if(x >= lb && x <= ub){
				return pi;
		}
		else if (x > ub){
				return 1;
		}
};

module.exports = PredModel;
