/* The inference engine used by inferServlet.js. The goal is to put an accurate
 * tag for each submitted event. 

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var gridStore = require('mongodb').GridStore;
//var act = require('./actuator');
var http = require("http");
var async = require("async");
var fs = require('fs');

module.exports = {
	batchProc: function(classname, events, db, collection, callback){
		var _this = this;
		console.log(classname+' recognition for '+events.length+' events');

		if (typeof classname !== 'undefined' && classname === 'speech'){
			// cannot use the for loop due to async
			async.eachLimit(events, 5, function(item, cb){
				_this.ASR(item, cb, db, collection);
			}, function(err){
				console.log('async finished');

				callback(events);
			});
		} else if (typeof classname !== 'undefined' && classname !== 'speech' && classname !== 'none'){
			async.each(events, function(item, cb){
				_this.AER(item, cb, db, collection, classname);
			}, function(err){
				console.log('async finished');

				callback(events);
			});
		}
		else{
			callback(events);	
		}
	},
	// auto speech recog
	ASR: function(eventj, done, db, collection){
		console.log('ASR');

		// check for existing tag, if true then skip
		if ( eventj.hasOwnProperty('tag') ){
			console.log('already tagged');
			done();
		}

		// get the raw data from the db for processing
		console.log(eventj.filename);
		var gs = new gridStore(db, eventj.filename, 'r', {
			'root': 'data' // assume that the raw data collection's name is known
		});
		console.log('new gridStore created');
		gs.open(function(err, gs){
			if (err){
				console.log(err);
				done();
			}
			else{
				gs.seek(0, function(){
					gs.read(function(err, data){
						if (err){
							console.log(err);
							done();
						}
						else{
							console.log('google ASR');
							var options = {
								hostname: 'www.google.com',
								path: '/speech-api/v2/recognize?key=AIzaSyD5NvcrQ54Rbzdxpo3FtJsAyvUjy6O3cn4&output=json&lang=en-us',
								method: 'POST',
								headers:{
									'Content-Type':'audio/l16; rate=16000;'
								}
							};

							var req = http.request(options, function(google_res){
								console.log('STATUS: '+google_res.statusCode);
								console.log('HEADERS: '+JSON.stringify(google_res.headers));
								google_res.setEncoding('utf8');
								
								var chunks = "";
								google_res.on('data', function (chunk) {
									chunks += chunk;
								});

								google_res.on('end', function(){
									console.log('google responses: '+chunks);
									var rawRes = chunks.split('\n');// only the second chunk contains valid responses.
									if (! rawRes[1])
										done();
									else{
										rawRes = JSON.parse(rawRes[1]);
										var recogWords = [];
										if (rawRes.result.length>0){
											for (var l = 0; l < rawRes.result[0].alternative.length; l++){
												// alternative[0] also has confidence level
												recogWords.push(rawRes.result[0].alternative[l].transcript);
											}
											console.log(recogWords);
											var sentence = recogWords.join(' ');

											eventj.tag = 'speech '+sentence;
											done();
										}
									}
								});
							});

							req.on('error', function(err){
								console.log(err);
								done();
							});
							req.write(data);
							req.end();
						}
					});
				});
			}	
		});
	},
	// acoustic event recognition
	AER: function(eventj, done, db, collection, classname){
		console.log('AER');

		// check for existing tag, if true then skip
		if ( eventj.hasOwnProperty('tag') ){
			console.log('already tagged');
			done();
		}

		// get the raw data from the db for processing
		console.log(eventj.filename);
		var gs = new gridStore(db, eventj.filename, 'r', {
			'root': 'data' // assume that the raw data collection's name is known
		});
		console.log('new gridStore created');
		gs.open(function(err, gs){
			if (err){
				console.log(err);
				done();
			}
			else{
				gs.seek(0, function(){
					gs.read(function(err, data){
						if (err){
							console.log(err);
							done();
						}
						else{
							console.log('custom AER');
							// write the data to a wav file
							console.log('writing the raw data under ./libs/data/')
							var wstream = fs.createWriteStream('./libs/data/audio.wav', {'encoding':'binary'});
							wstream.write(data);
							wstream.end();
							console.log('wav file written');

							done();
						}
					});
				});
			}	
		});
	}
};

