/* SAS client library
 * The MIT License
 *
 * Copyright 2015.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Author: Long Le <longle1@illinois.edu>
 */

var protocol = 'http://';
var port = ':8080';

// access the remote service to check its status
var IllStatusGet = function(serviceAddr, cb_done, cb_fail){
	$.ajax({
		url: protocol+serviceAddr+port+'/',
		type:'GET',
		timeout: 600000
	}).done(function(resp){
		console.log(resp);
		cb_done();
	}).fail(function(){
		console.log('ajax failed');
		cb_fail();
	});
};
// access the remote service to get the model list
var IllModelGet = function(serviceAddr, cb_done, cb_fail){
	$.ajax({
		url: protocol+serviceAddr+port+'/model',
		type:'GET',
		timeout: 600000
	}).done(function(resp){
		console.log(resp);
		cb_done(resp);
	}).fail(function(){
		console.log('ajax failed');
		cb_fail();
	});
};
// access the remote service to update events
var IllColPut = function(serviceAddr, db, col, user, pwd, filename, op, field){
    var qStr = $.param({'dbname':db, 'colname':col, 'user':user, 'passwd': pwd});
    
	$.ajax({
		url: protocol+serviceAddr+port+'/col?'+qStr,
		type:'PUT',
		data: '{"filename":"'+filename+'"}\n{"$'+op+'":'+field+'}',
		dataType: 'text',
		timeout: 600000
		//xhrFields: {
		//   withCredentials: true
		//}
	}).done(function(resp){
		console.log(resp);
	}).fail(function(){
		console.log('ajax failed');
	});
};

// access the remote service to get binary data
var IllGridGet = function(serviceAddr, db, user, pwd, gridCol, filename, cb_done, cb_fail){
    var qStr = $.param({'dbname':db, 'colname': gridCol, 'user':user, 'passwd': pwd, 'filename': filename});
    
	$.ajax({
		url: protocol+serviceAddr+port+'/gridfs?'+qStr,
		type:'GET',
		dataType :'arraybuffer',
		timeout: 600000
		//xhrFields: {
		//    withCredentials: true
		//}
	}).done(function(data){
		cb_done(data);
	}).fail(function(){
		console.log('ajax failed');
		cb_fail();
	});
};

// access the remote service to query
var IllQuery = function (serviceAddr, db, user, pwd, col, q, cb_done, cb_fail){
    var reqForm = formReq(db,user,pwd, col,q);

	$.ajax({
		url: protocol+serviceAddr+port+'/query?'+reqForm[0],
		type:'POST',
		data: reqForm[1],
		dataType: 'text',
		timeout: 600000
  	}).done(function(data){
	  	var events = JSON.parse(data);
		cb_done(events);
  	}).fail(function(){
	  	console.log('ajax failed');
		cb_fail();
  	});
};

/*
// access the remote service for inference
var IllInferPost = function (serviceAddr, db, col, user, pwd, classname, eventj, cb_done, cb_fail){
    var qStr = $.param({'dbname':db, 'colname': col, 'user': user, 'passwd': pwd, 'classname': classname});

	$.ajax({
		url: protocol+serviceAddr+port+'/infer?'+qStr,
		type:'POST',
		data: JSON.stringify(eventj),
		dataType: 'json',
		timeout: 600000
	}).done(function(eventj){
		cb_done(eventj);
	}).fail(function(){
		console.log('ajax failed');
		cb_fail();
	});
};

// access the remote service to register for new events
var IllQueryPostReg = function (serviceAddr, db, user, pwd, col, q, cbReg){
    var reqForm = formReq(db,user,pwd, col,q);

	$.ajax({
		url: protocol+serviceAddr+port+'/query/register?'+reqForm[0],
		type:'POST',
		data: reqForm[1],
		dataType: 'text',
		timeout: 600000
	}).done(function(data){
		console.log('registration at '+serviceAddr+' is successful!');
		//alert('registration successful!');

		// do not wait for all services since their data are pushed asynchronously
		socketList[serviceAddr] = io.connect(protocol+serviceAddr+port, {forceNew:true, reconnection:false});
		socketList[serviceAddr].on('connect', function(){
			socketList[serviceAddr].on('newResp', function(events){
				cbReg(events); 
			});
		});
	}).fail(function(){
		console.log('unable to register');
	});
};
*/
