/* Utilities function of acousticSearch.html
 * The MIT License
 *
 * Copyright 2015.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Author: Long Le <longle1@illinois.edu>
 */

var url = window.location.href;
var arr = url.split('/');
var currServiceAddr = arr[2];
var DB = 'publicDb';
var USER = null;
var PWD = 'publicPwd';
var map;
var circle;
var oms;
var iw;
var chart;
var chartOptions;
var dataTable;
//var dateDtIdx, scoreDtIdx, annoDtIdx, ttipDtIdx;
//var filenameIdx=0, scoreIdx=1, tagIdx=2;
var audioCtx = new AudioContext(); // should only have 1 per doc
var streamTimerId;
//var socketList = [];
var omsService;
var thresh = 0;
//var serviceTimerId;

// perform acoustic search using the query-service
var acousticSearch = function (cb){
    var q = getQuery(),
        serviceAddr = currServiceAddr;

    $("html").css("cursor", "progress");
    IllQuery(serviceAddr, DB, USER, PWD, 'event', q, 
            function(events){
                $("html").css("cursor", "default");
                displayEvent(events, true);
                typeof cb === 'function' && cb();
            }, function(){
                $("html").css("cursor", "default");
                console.log("query failed");
                $( "#pStatus" ).text('Query failed!').show().fadeOut(1000, "linear");
                typeof cb === 'function' && cb();
            });
}

var acousticPull = function(){
    // restart the interval timer
    clearInterval(streamTimerId);
    streamTimerId = setInterval(function(){
        var currDate = new Date();
        var prevDate = new Date(currDate);
        prevDate.setTime(prevDate.getTime() - parseInt($("#winlen").val()));
        $("#t1").val(prevDate.toISOString());
        $("#t2").val(currDate.toISOString());
        acousticSearch();
    }, parseInt($("#period").val()));
}

// query acoustic service using the push mechanism
/*
   var acousticReg = function (){
   var q = getQuery(),
   serviceAddr = currServiceAddr;

   if (socketList[serviceAddr]){
   socketList[serviceAddr].disconnect();
   }

   IllQueryPostReg(serviceAddr, DB, USER, PWD, 'event', q, 
   function(events){
   displayEvent(events,false);
   });
   }
   */

// init panel
var initPanel = function(){
    $("#serviceAddr").val(currServiceAddr);
    //$("#serviceAddr").unbind('change');
    $("#serviceAddr").change(function(){
        // check the status of the service first
        IllStatusGet($("#serviceAddr").val(), function(){
            currServiceAddr = $("#serviceAddr").val();
            displayService(true);
            console.log('Current service address is '+currServiceAddr);
        }, function(){
            alert('The new service does not exist or is offline, rollback');
            $("#serviceAddr").val(currServiceAddr);
        });
    });
    $("#DB").val(DB);
    //$("#USER").val(USER);
    $("#PWD").val(PWD);
    $("#dur1").val(0);
    $("#dur2").val(10);
    $("#f1").val(0);
    $("#f2").val(8000);

    // model discovery
    /*
       IllModelGet(currServiceAddr,function(availModels){
       $.each(availModels, function(idx, model){
       if ($("#classes option[value="+model+"]").length <= 0){
       $('#classes').append($('<option/>',{
       value: model,
       text : model 
       }));
       }
       });
       }, function(){
       console.log('Cannot find any models to initialize the panel')
       });
       */

    // search mode
    if ($("#searchMode").is(':checked')){
        $("#labelt1").html("From");
        $("#labelt2").html("To");

        var currDate = new Date();
        var prevDate = new Date();
        prevDate.setDate(currDate.getDate()-1);
        $("#t1").show();
        $("#t2").show();
        $("#period").hide();
        $("#winlen").hide();
        $("#t1").val(prevDate.toISOString());
        $("#t2").val(currDate.toISOString());

        clearInterval(streamTimerId);
        $("#button1").show();
        $("#button1").val("Search");
        $("#button1").click(function(){
            $("#button1").attr('disabled',true);
            acousticSearch(function(){
                $("#button1").attr('disabled',false);
            });
        });
        /*
           $("#seqText").show();
           $("#button2").show();
           $("#button2").val("Remove if below");
           $("#button2").attr("onclick","prune()");
        // event threshold
        $("#thresh").val(thresh).change(function(){
        thresh = $("#thresh").val();
        }).show();
        */

        cleanDisplay();
        // stream mode
    } else if ($("#streamMode").is(':checked')){	
        $('#labelt1').html('Update rate (ms)');
        $('#labelt2').html('Window length (ms)');

        $("#t1").hide();
        $("#t2").hide();
        $("#period").show();
        $("#winlen").show();
        $("#winlen").val("600000"); // 10 min
        $("#period").val("10000").change(acousticPull);

        $("#button1").hide();
        /*
           $("#seqText").hide();
           $("#button2").hide();
           $("#thresh").hide();
           */
        acousticPull();

        cleanDisplay();
        /*
        // push mode
        } else if ($("#pushMode").is(':checked')){
        $('#labelt1').html('From');
        $('#labelt2').html('To');

        var currDate = new Date();
        var prevDate = new Date();
        var nextDate = new Date();
        prevDate.setDate(currDate.getDate()-1);
        nextDate.setDate(currDate.getDate()+1);
        $("#t1").show();
        $("#t2").show();
        $("#period").hide();
        $("#winlen").hide();
        $("#t1").val(prevDate.toISOString());
        $("#t2").val(nextDate.toISOString());

        clearInterval(streamTimerId);
        $("#button1").hide();
        $("#button2").hide();
        $("#thresh").hide();
        acousticReg();

        cleanDisplay();
        */
    } else{
        throw "unable to init"
    }

    // interactive display
    setInterval(function(){
        var d = new Date();
        $('#time').html('Time '+d.toISOString());
    },1000);
};

// init map
var initMap = function(){
    //Geographic Center of the US: https://en.wikipedia.org/wiki/Geographic_center_of_the_contiguous_United_States
    $("#lat").val("39.828175");
    $("#lng").val("-98.5795");
    $("#rad").val("2000"); // miles

    // space visualization
    var gLoc = [$('#lat').val(), $('#lng').val()];
    var gRad = $('#rad').val();

    var mapProp = {
        center:new google.maps.LatLng(gLoc[0], gLoc[1]),
        zoom:Math.round(Math.log($(window).width()/512))+1,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    circle = new google.maps.Circle({
        strokeColor: '#FF0000',
           strokeOpacity: 0.8,
           strokeWeight: 2,
           fillColor: '#FF0000',
           fillOpacity: 0.35,
           map: map,
           draggable: false,
           center: new google.maps.LatLng(gLoc[0], gLoc[1]),
           radius: gRad*1609.34 // 1 mile = 1609.34 meters
    });

    $("#lat").change(function(){
        circle.setCenter(new google.maps.LatLng($("#lat").val(), $("#lng").val()));
        //reset();
    });
    $("#lng").change(function(){
        circle.setCenter(new google.maps.LatLng($("#lat").val(), $("#lng").val()));
        //reset();
    });
    $("#rad").change(function(){
        circle.setRadius($("#rad").val()*1609.34);
        //reset();
    });

    iw=new google.maps.InfoWindow();
    oms=new OverlappingMarkerSpiderfier(map);

    oms.addListener('click', function(marker) {
        var filename = JSON.parse(marker.info).filename;
        //var serviceAddr = info[3];
        var serviceAddr = currServiceAddr;

        // Download sound data and play
        IllGridGet(serviceAddr, DB, USER, PWD, 'data', filename, function(data){
            console.log('data.byteLength = '+data.byteLength);
            soundPlay(data,marker);
        }, function(){
            console.log('cannot download data');
            alert('cannot download data');
        });
    });

    omsService = new OverlappingMarkerSpiderfier(map);

    displayService(true);
    /*
       getServiceAddr();
       clearInterval(serviceTimerId);
       serviceTimerId = setInterval(getServiceAddr, 3600000); // every 1 hour
       */
};

/*
// get the list of all service address from terra.eecs.berkeley.edu
var getServiceAddr = function(){
// clean up first
serviceAddrList = [];
socketList = [];
var markers = omsService.getMarkers();
for (var i = 0; i <markers.length;i++){
markers[i].setMap(null);
}
omsService.clearMarkers();

var hosts = {};
hosts['acoustic.ifp.illinois.edu'] = {'ip':'130.126.122.247'};
$.getJSON("https://terra.eecs.berkeley.edu:8088/hosts", function(swarmHosts){
// append the default host
console.log('query terra.eecs succeeded');
var keyArray = Object.keys(swarmHosts);
for (var k=0; k<keyArray.length; k++){
hosts[keyArray[k]] = {'ip':swarmHosts[keyArray[k]].ip};
}
}).fail(function(){
console.log('query terra.eecs failed');
}).always(function(){
console.log('display all hosts');

var keyArray = Object.keys(hosts);
async.every(keyArray, function(item, cb){
// check the status of the service first
IllStatusGet(hosts[item].ip, function(){
// if ok then add to the list
serviceAddrList.push(hosts[item].ip);
socketList.push(null);
cb(true);
}, function(){
cb(true);
});
}, function(result){
// display available service on the map too
for (var i=0; i < serviceAddrList.length; i++){
// look up location using ip
//$.getJSON("https://www.telize.com/geoip/"+serviceAddrList[i]+"?callback=?", function (data){
$.getJSON("https://www.freegeoip.net/json/"+serviceAddrList[i], function (data){
// Create a marker for each swarmbox
var marker = new google.maps.Marker({
position:new google.maps.LatLng(data.latitude, data.longitude),
icon:'images/computers.png', // Maps Icons Collection https://mapicons.mapsmarker.com
zIndex:0,
info: data.ip
});
marker.setMap(map);
google.maps.event.addListener(marker, 'mouseover', (function(marker) {
return function(){
iw.setContent(marker.info);
iw.open(map, marker);
};
})(marker));
google.maps.event.addListener(marker, 'mouseout', function(){
iw.close();
});
omsService.addMarker(marker);
});
}
});
});
};
*/

// init chart
var initChart = function(){
    // time visualization
    chart = new google.visualization.LineChart(document.getElementById('googleChart'));
    chartOptions = {
        legend: 'none',
        lineWidth: 0,
        pointSize: 5,
        hAxis: {
            title: 'Local time',
            format: 'yyyy/MM/dd HH:mm:SSSS',
            gridlines: {count: -1}
        },
        vAxis: {
            title: 'Value',
            gridlines: {color: 'none'},
            minValue: 0
        }
    };
    dataTable = new google.visualization.DataTable();
    // column types
    dataTable.addColumn('date', 'Date');
    dataTable.addColumn('number', 'Value');
    dataTable.addColumn({type:'string', role:'annotation'});
    dataTable.addColumn({type:'string', role:'tooltip'});
    //dataTable.addColumn('string', 'tag');
    //dataTable.addColumn('string', 'filename');
    //dataTable.addColumn('string', 'serviceAddr');

    // column indexes
    /*
       dateDtIdx = 0;
       scoreDtIdx = 1;
       annoDtIdx = 2;
       ttipDtIdx = 3;
       */
    google.visualization.events.addListener(chart, 'select', function(){
        var selectedItem = chart.getSelection()[0];
        if (selectedItem) {
            var marker = {};
            marker.selectedItem = selectedItem;
            var filename = JSON.parse(dataTable.getValue(marker.selectedItem.row, 3)).filename;
            //var serviceAddr = info[3];
            var serviceAddr = currServiceAddr;

            // Download sound data and play
            IllGridGet(serviceAddr, DB, USER, PWD, 'data', filename, function(data){
                console.log('data.byteLength = '+data.byteLength);
                soundPlay(data,marker);
            }, function(){
                console.log('cannot download data');
                alert('cannot download data');
            });
        }
    });
};

// get query from the current form
var getQuery = function (){
    var q = {};

    DB = $("#DB").val();
    //USER = $("#USER").val();
    PWD = $("#PWD").val();

    q.t1 = $("#t1").val();
    q.t2 = $("#t2").val();

    if ($("#f1").val()){
        q.f1 = $("#f1").val();
    }
    if ($("#f2").val()){
        q.f2 = $("#f2").val();
    }

    if ($("#dur1").val()){
        q.dur = $("#dur1").val();
    }
    if ($("#dur2").val()){
        q.dur2 = $("#dur2").val();
    }

    if ($("#lat").val() && $("#lng").val()){
        q.loc = [$("#lat").val(), $("#lng").val()];
    }
    if ($("#rad").val()){
        q.rad = $("#rad").val();
    }
    if ($("#tag").val()){
        q.tag = $("#tag").val();
    }
    if ($("#filename").val()){
        q.filename = $("#filename").val();
    }
    /*
       if ($('input[name="infer"]:checked').val() === "tag"){
       if ($("#kw").val()){
       q.kw = $("#kw").val();
       }
       console.log($("#classes").val());
       if ($("#classes").val()){
       q.cname = $("#classes").val();
       }
       }
       */

    return q;
};

// form request to remote service using the user query
var formReq = function(db, user, pwd, col, q){
    //var tZoneOffset = 5/24;
    var strArr = [];

    // Construct the query data to send
    var timeDat;
    if (q.hasOwnProperty('t1') && q.hasOwnProperty('t2')){
        timeDat = '{"recordDate":{"$gte":{"$date":"'+ q.t1+'"}, "$lte":{"$date":"'+q.t2+'"}}}';
    }
    else if (q.hasOwnProperty('t1')){
        timeDat = '{"recordDate":{"$gte":{"$date":"'+ q.t1+'"}}}';
    }
    else if (q.hasOwnProperty('t2')){
        timeDat = '{"recordDate":{"$lte":{"$date":"'+ q.t2+'"}}}';
    } else{
        timeDat = [];
    }
    strArr = strArr.concat(timeDat);

    var freqDat;
    if (q.hasOwnProperty('f1') && q.hasOwnProperty('f2')){
        freqDat = '{"minFreq":{"$gte":'+q.f1+'}},{"maxFreq":{"$lte":'+q.f2+'}}';
    }
    else if (q.hasOwnProperty('f1')){
        freqDat = '{"minFreq":{"$gte":'+q.f1+'}}';
    }else if (q.hasOwnProperty('f2')){
        freqDat = '{"maxFreq":{"$lte":'+q.f2+'}}';
    }else{
        freqDat = [];
    }
    strArr = strArr.concat(freqDat);

    var durDat;
    if (q.hasOwnProperty('dur1') && q.hasOwnProperty('dur2')){
        durDat = '{"maxDur":{"$gte":'+q.dur1+', "$lte":'+q.dur2+'}}';
    }
    else if (q.hasOwnProperty('dur1')){
        durDat = '{"maxDur":{"$gte":'+q.dur1+'}}';
    }
    else if (q.hasOwnProperty('dur2')){
        durDat = '{"maxDur":{"$lte":'+q.dur2+'}}';
    }
    else{
        durDat = [];
    }
    strArr = strArr.concat(durDat);

    /*
       var lpDat;
       if (q.hasOwnProperty('lp1') && q.hasOwnProperty('lp2')){
       lpDat = '{"logProb":{"$gte":'+q.lp1+', "$lte":'+q.lp2+'}}';
       }
       else if (q.hasOwnProperty('lp1')){
       lpDat = '{"logProb":{"$gte":'+q.lp1+'}}';
       }
       else if (q.hasOwnProperty('lp2')){
       lpDat = '{"logProb":{"$lte":'+q.lp2+'}}';
       }
       else{
       lpDat = '';
       }
       */

    var locDat;
    if (q.hasOwnProperty('loc') && q.hasOwnProperty('rad')){
        locDat = '{"location":{"$geoWithin":{"$centerSphere":[['+q.loc[1]+','+q.loc[0]+'], '+q.rad/3959+']}}}'; // earthRad = 3959 miles
    }else{
        locDat = [];
    }
    strArr = strArr.concat(locDat);

    var tagDat;
    if (q.hasOwnProperty('tag')){
        tagDat = '{"$text": {"$search":"'+q.tag+'"}}';
    }else{
        tagDat = [];
    }
    strArr = strArr.concat(tagDat);

    var filenameDat;
    if (q.hasOwnProperty('filename')){
        filenameDat = '{"filename": "'+q.filename+'"}';
    }else{
        filenameDat = [];
    }
    strArr = strArr.concat(filenameDat);

    /*
       if ($("#seq").prop('checked')){
       var filenameDat;
       var markers = oms.getMarkers();
       if (markers.length > 0){
       filenameDat = '{"filename":{"$in":[';

       for (var i = 0; i <markers.length;i++){
       filenameDat += '"'+JSON.parse(markers[i].info).filename+'",';
       }
       filenameDat = filenameDat.slice(0,-1);

       filenameDat += ']}}';
       } else{
       filenameDat = [];
       }
       strArr = strArr.concat(filenameDat);
       }
       */

    var postDat = '[{"$and":['+strArr.join(',')+']},{}]';
    // Construct the query string
    var qStr = $.param({'dbname':db, 'colname': col, 'user': user, 'passwd': pwd});

    return [qStr, postDat];
}

// display service
var displayService = function(isClean){
    if (isClean){
        // clean old events
        var markers = omsService.getMarkers();
        for (var i = 0; i <markers.length;i++){
            markers[i].setMap(null);
        }
        omsService.clearMarkers();
    }

    var serviceAddr = currServiceAddr;
    $.getJSON("https://www.freegeoip.net/json/"+serviceAddr, function (data){
        // Create a marker for each swarmbox
        var marker = new google.maps.Marker({
            position:new google.maps.LatLng(data.latitude, data.longitude),
            icon:'images/computers.png', // Maps Icons Collection https://mapicons.mapsmarker.com
            zIndex:0,
            info: data.ip
        });
        marker.setMap(map);
        google.maps.event.addListener(marker, 'mouseover', (function(marker) {
            return function(){
                iw.setContent(marker.info);
                iw.open(map, marker);
            };
        })(marker));
        google.maps.event.addListener(marker, 'mouseout', function(){
            iw.close();
        });
        omsService.addMarker(marker);
    });
}

var cleanDisplay = function(){
    // clean old events
    var markers = oms.getMarkers();
    for (var i = 0; i <markers.length;i++){
        markers[i].setMap(null);
    }
    oms.clearMarkers();
    dataTable.removeRows(0, dataTable.getNumberOfRows());
}

// display ranked events on the map and the chart
var displayEvent = function(events, isClean){
    console.log('displayEvent');
    if (isClean){
        cleanDisplay()
    }

    console.log('# events '+ events.length);

    if (events.length > 0){
        var numField = $("#numField").val();

        scoreArr = events.map(function(aEvent){
            if  (numField in aEvent && !isNaN(aEvent[numField])){
                return aEvent[numField]
            }
            return 0
        });

        var maxScore = math.max(scoreArr);
        var minScore = math.min(scoreArr);
        var numMarkers = oms.getMarkers().length;
        for (var i = 0; i <events.length;i++){
            mDict = {}
            mDict["filename"] = events[i].filename
            mDict[numField] = scoreArr[i].toFixed(4)
            mDict["tag"] = events[i].tag
            // create a marker in the map for each event
            var marker = new google.maps.Marker({
                position:new google.maps.LatLng(parseFloat(events[i].location.coordinates[1]), parseFloat(events[i].location.coordinates[0])),
                icon:'//chart.googleapis.com/chart?chst=d_map_pin_letter&chld='+ (numMarkers+i) +'|'
                +num2hexcolor(scoreArr[i], minScore, maxScore)+'|FFFFFF', // fill color|text color
                zIndex:google.maps.Marker.MAX_ZINDEX + 1,
                info: JSON.stringify(mDict)
            });
            marker.setMap(map);

            google.maps.event.addListener(marker, 'mouseover', (function(marker) {
                return function(){
                    iw.setContent(marker.info.replace(/(?:\r\n|\r|\n)/g, '<br />'));
                    iw.open(map, marker);
                };
            })(marker));
            google.maps.event.addListener(marker, 'mouseout', function(){
                iw.close();
            });
            oms.addMarker(marker);

            // create a row in the dataTable of the chart for each event
            dataTable.addRow([
                    new Date(events[i].recordDate), 
                    scoreArr[i], 
                    (numMarkers+i).toString(), 
                    JSON.stringify(mDict)
                    ]);
        }
    } else{
        $( "#pStatus" ).text('No events matched the given criteria!').show().fadeOut(1000, "linear");
    }
    var view = new google.visualization.DataView(dataTable);
    //view.setColumns([dateDtIdx,scoreDtIdx,annoDtIdx,ttipDtIdx]);
    view.setColumns([0,1,2,3]);
    chart.draw(view, chartOptions);
};

// prune markers/events based on the threshold
var prune = function(){
    var markers = oms.getMarkers();
    for (var i = 0; i <markers.length;i++){
        if (JSON.parse(markers[i].info).value < thresh){
            markers[i].setMap(null);
            oms.removeMarker(markers[i]);
        }
    }

    var i = 0;
    while (i < dataTable.getNumberOfRows()){
        if (JSON.parse(dataTable.getValue(i,3)).value < thresh){
            dataTable.removeRow(i);
        }else{
            i++;
        }
    }
    var view = new google.visualization.DataView(dataTable);
    view.setColumns([0,1,2,3]);
    chart.draw(view, chartOptions);

    console.log('pruned');
}


/*
   var parseInfo = function(str,key){
   var info = str.split('\n');
   return info[key];
   }
   */

var chgInfo = function(str,key,val){
    var info = JSON.parse(str);
    info[key] = val;
    return JSON.stringify(info);
}

// convert a numerical value to a color
var num2hexcolor = function(val, min, max){
    // cap the input value
    if (val > max)
        val = max;
    else if (val < min)
        val = min;
    // the 5 color heatmap algorithm
    h = math.round((1.0 - val/max) * 100);
    l = math.round(val/max*50);
    var color = tinycolor("hsl("+h+"%, 100%, "+l+"%)");
    return color.toHex();
};

// decode data, then play the sound and call the callback at the end
var soundPlay = function(data,marker){
    audioCtx.decodeAudioData(data, function(buf){
        // play the sound
        var source = audioCtx.createBufferSource();
        source.buffer = buf;
        source.connect(audioCtx.destination);
        source.start(0);

        promptUserWith(data,marker);
    }, function(error){
        console.log('audio decoding error');
    });
};

/*
// get init tag	
var getInitTag = function(marker){
var eventj = {};
var serviceAddr;
var info;

if (typeof marker.selectedItem === 'undefined'){
info = parseInfo(marker.info);
} else{
info = parseInfo(dataTable.getValue(marker.selectedItem.row, 3));
}
eventj.filename = info[filenameIdx];
//serviceAddr = info[3];
serviceAddr = currServiceAddr;

IllInferPost(serviceAddr, DB, 'event', USER, PWD, $('#classes').val(), eventj,
function(new_events){
if (typeof marker.selectedItem === 'undefined'){
//marker.tag = new_events[0].tag;
chgInfo(marker.info,3,new_events[0].tag);
} else{
//dataTable.setValue(marker.selectedItem.row, tagDtIdx, new_events[0].tag);
chgInfo(dataTable.getValue(marker.selectedItem.row, 3),3,new_events[0].tag);
}
promptUserWith(marker);
}, function(){
promptUserWith(marker);
});
promptUserWith(marker);
}
*/

// prompt user with marker info for confirmation/edition
var promptUserWith = function(data,marker){
    var filename, serviceAddr;
    //var initTag
    var info;

    if (typeof marker.selectedItem === 'undefined'){
        info = JSON.parse(marker.info);
    } else{
        info = JSON.parse(dataTable.getValue(marker.selectedItem.row, 3));
    }
    filename = info.filename;
    //initTag = info.tag;
    //serviceAddr = info[3];
    serviceAddr = currServiceAddr;

    var newTag;
    var newKey;
    //var newTag = initTag;
    //newTag = prompt("Modify tags", initTag);
    //$("#dialog-event").attr('title', filename);
    //$("#inputTag").val(initTag);
    var dlg = $( "#dialog-event" ).dialog({
        resizable: false,
        //autoOpen: false,
        title: filename,
        width: 384,
        modal: true,
        buttons: {
            "Update": function() {
                newKey = $("#inputKey").val();
                newTag = $("#inputTag").val();
                console.log('newKey = '+newKey);
                console.log('newTag = '+newTag);
                if (newKey && newTag){
                    if (isNaN(newTag)){
                        IllColPut(serviceAddr, DB, 'event', USER, PWD, filename, "set", '{"'+newKey+'":"'+newTag+'"}');
                    }else{
                        IllColPut(serviceAddr, DB, 'event', USER, PWD, filename, "set", '{"'+newKey+'":'+newTag+'}');
                        }

                        if (newKey == 'tag'){
                            if (typeof marker.selectedItem === 'undefined'){
                                // update the map marker
                                //marker.tag = newTag;
                                var newInfo = chgInfo(marker.info,'tag',newTag);
                                marker.info = newInfo;
                            } else{
                                // update the chart point
                                //dataTable.setValue(marker.selectedItem.row, tagDtIdx, newTag);
                                //var tt = dataTable.getValue(marker.selectedItem.row, 3);
                                //var tt_lines = tt.split('\n');
                                //tt_lines[tt_lines.length-1] = newTag; //hardcoded idx of tag
                                var newInfo = chgInfo(dataTable.getValue(marker.selectedItem.row, 3),'tag',newTag);
                                dataTable.setValue(marker.selectedItem.row, 3, newInfo);


                                var view = new google.visualization.DataView(dataTable);
                                //view.setColumns([dateDtIdx,scoreDtIdx,annoDtIdx,ttipDtIdx]);
                                view.setColumns([0,1,2,3]);
                                chart.draw(view, chartOptions);
                            }
                        }
                    }

                    $( this ).dialog( "close" );
                },
                   "Download": function() {
                       //var blob = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
                       var dataView = new DataView(data);
                       var blob = new Blob([dataView], { type: "mimeString" });
                       saveAs(blob, filename);

                       $( this ).dialog( "close" );
                   },
                   "Cancel": function() {
                       $( this ).dialog( "close" );
                   }
            }
        });
    //dlg.dialog( "open" );
    }

