/* Philip hue light

  MIT License (MIT)

  Copyright (c) 2015 Long Le

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
	
	Authors: Long Le <longle1@illinois.edu>
*/

var BRIDGEIP='128.174.226.140';
var USERNAME='760f6fe5759c473d26b123f29e86b';
var LIGHTID='2';
/*
var options = {
	host:BRIDGEIP,
	path: '/api/'+USERNAME+'/lights/'+LIGHTID+'/state',
	method: 'PUT',
	headers:{
		'Content-Type':'application/json'
	}
};
*/

var lightActuate = function(sentence){
	console.log('actuator');

	console.log(sentence);
	if (sentence){
		var inferClass = sentence.split(' '); inferClass = inferClass[0];
		if (inferClass==='speech'){
			if (sentence.indexOf('light')>-1&&sentence.indexOf('on')>-1||sentence.indexOf('hello')>-1){
				console.log('lightOn');
				lightOn();
			}
			if (sentence.indexOf('light')>-1&&sentence.indexOf('off')>-1||sentence.indexOf('bye')>-1){
				console.log('lightOff');
				lightOff();
			}
			if (sentence.indexOf('light')>-1&&sentence.indexOf('red')>-1){
				console.log('light red');
				lightColor('red');
			}
			if (sentence.indexOf('light')>-1&&sentence.indexOf('yellow')>-1){
				console.log('light yellow');
				lightColor('yellow');
			}
			if (sentence.indexOf('light')>-1&&sentence.indexOf('green')>-1){
				console.log('light green');
				lightColor('green');
			}
			if (sentence.indexOf('light')>-1&&sentence.indexOf('blue')>-1){
				console.log('light blue');
				lightColor('blue');
			}
			if (sentence.indexOf('light')>-1&&sentence.indexOf('purple')>-1){
				console.log('light purple');
				lightColor('purple');
			}
		}
		else if (inferClass==='gun_shot'){
				console.log('lightDanger');
				lightDanger(0); // red
		}
	}
}

var lightOn = function(){
	console.log('lightOn');
    
	$.ajax({
			url: '//'+BRIDGEIP+'/api/'+USERNAME+'/lights/'+LIGHTID+'/state',
			type:'PUT',
			headers:{
			'Content-Type':'application/json'
			},
			data: '{"on": true}',
			dataType: 'text',
			timeOut: 10000,
	}).done(function(data){
			console.log(data);
	}).fail(function(err){
			console.log(err);
	});
};

var lightOff = function(){
	console.log('lightOff');

	$.ajax({
			url: '//'+BRIDGEIP+'/api/'+USERNAME+'/lights/'+LIGHTID+'/state',
			type:'PUT',
			headers:{
			'Content-Type':'application/json'
			},
			data: '{"on": false}',
			dataType: 'text',
			timeOut: 10000,
	}).done(function(data){
			console.log(data);
	}).fail(function(err){
			console.log(err);
	});
};

var lightColor = function(colStr){
	console.log('lightColor');

	var hueVal;
	switch (colStr){
		case 'red':
			hueVal = 0;
			break;
		case 'yellow':
			hueVal = 12750;
			break;
		case 'green':
			hueVal = 25500;
			break;
		case 'blue':
			hueVal = 46920;
			break;
		case 'purple':
			hueVal = 56100;
			break;
	}
	$.ajax({
			url: '//'+BRIDGEIP+'/api/'+USERNAME+'/lights/'+LIGHTID+'/state',
			type:'PUT',
			headers:{
			'Content-Type':'application/json'
			},
			data: '{"on": true, "hue":'+hueVal+'}',
			dataType: 'text',
			timeOut: 10000,
	}).done(function(data){
			console.log(data);
	}).fail(function(err){
			console.log(err);
	});
};

var lightDanger = function(hueVal){
	console.log('lightDanger');

	$.ajax({
			url: '//'+BRIDGEIP+'/api/'+USERNAME+'/lights/'+LIGHTID+'/state',
			type:'PUT',
			headers:{
			'Content-Type':'application/json'
			},
			data: '{"on":true, "hue":'+hueVal+', "alert":"select"}',
			dataType: 'text',
			timeOut: 10000,
	}).done(function(data){
			console.log(data);
	}).fail(function(err){
			console.log(err);
	});
};

