#!/bin/bash

# replace your desirable iterface name instead of 'eth0'
IPADDR=$(ifconfig eth0 | awk '/t addr:/{gsub(/.*:/,"",$2);print$2}')
forever start -o out.log -e err.log sasDbServer.js $IPADDR
