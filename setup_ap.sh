#! /bin/bash
# Must run with root
#
if [ ! -d ~/create_ap ]; then
	mkdir ~/create_ap
	git clone https://github.com/oblique/create_ap ~/create_ap
fi

sudo nmcli nm wifi off
sudo rfkill unblock wifi
sudo ~/create_ap/create_ap --stop wlan1

sudo ~/create_ap/create_ap --daemon wlan1 em1 swarmboxnet swarmboxpass
